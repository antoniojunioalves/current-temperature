# `Temperaturas`

Status do Projeto: Concluído :heavy_check_mark:

## `Descrição:`

Mostrar a temperatura de 3 cidades:

:white_check_mark: Nuuk/GL

:white_check_mark: Urubici/BR

:white_check_mark: Nairobi/KE

> [Live Demo](http://current-temperature.herokuapp.com/)

### `Pré-requisitos`

:memo: git

:memo: yarn

### `Como rodar a aplicação` :arrow_forward:

Clonar o projeto em um repositório local:

```bash
git clone https://gitlab.com/antoniojunioalves/current-temperature.git
```

Entre dentro da pasta do projeto:

```bash
cd current-temperature
```

Instale as dependências:

```bash
yarn
```

Execute a aplicação:

```bash
yarn start
```

Abrir a Url [http://localhost:3000](http://localhost:3000) para visualizar o projeto.

### `Como rodar os testes` :pencil:

Dentro do diretório do projeto, baixado conforme [Como rodar a aplicação](#como-rodar-a-aplicação)

```bash
yarn test
```

> Obs.: Para executar o teste com cobertura de código basta rodar o comando **yarn coverage**

### `Como criar o build para produção` :hammer:

Dentro do diretório do projeto, baixado conforme [Como rodar a aplicação](#como-rodar-a-aplicação)

```bash
yarn build
```

### `Dependências`

| Dependences                                              |                                                     |
| -------------------------------------------------------- | --------------------------------------------------- |
| :small_blue_diamond: testing-library/jest-dom: 5.11.4    | :small_blue_diamond: testing-library/react: ^11.1.0 |
| :small_blue_diamond: testing-library/user-event: 12.1.10 | :small_blue_diamond: date-fns: 2.21.1               |
| :small_blue_diamond: sass: 1.32.12                       | :small_blue_diamond: eslint: 7.25.0                 |
| :small_blue_diamond: eslint-config-prettier: 8.3.0       | :small_blue_diamond: eslint-plugin-prettier: 3.4.0  |
| :small_blue_diamond: eslint-plugin-react: 7.23.2         | :small_blue_diamond: fetch-mock: 9.11.0             |
| :small_blue_diamond: node-fetch: 2.6.1                   |                                                     |

Copyright :copyright: 2021 - Current Temperature
