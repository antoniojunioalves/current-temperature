const getWeather = async (city) => {
  var weather = await fetch(
    `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&APPID=c8d3490380afd9d61df0e0cdffedbd26`
  )
    .then((response) => {
      if (!response.ok) throw new Error();
      return response.json();
    })
    .then((response) => response);
  return weather;
};

export { getWeather };
