// External libs
import { render } from "@testing-library/react";

// Component
import Header from "./Header";

describe("Component <Header />", () => {
  test("should render component", () => {
    const component = render(<Header />);
    expect(component).toBeDefined();
  });

  test("Exists logo", () => {
    const { getByTestId } = render(<Header />);

    const imgLogo = getByTestId("header-logo");

    expect(imgLogo).toBeInTheDocument();
  });
});
