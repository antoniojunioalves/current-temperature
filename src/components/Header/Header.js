// External libs
import React from "react";

// CSS
import "./header.scss";

// Image
import Logo from "../../assets/images/logo.svg";

const App = () => {
  return (
    <header className="header-container">
      <img data-testid="header-logo" src={Logo} alt="Logo" />
    </header>
  );
};

export default App;
