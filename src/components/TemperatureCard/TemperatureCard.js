// External libs
import React, { useState, useEffect } from "react";

// CSS
import "./temperatureCard.scss";

// Components
import TryAgain from "./components/TryAgain";
import Details from "./components/Details";

// Images
import { ReactComponent as Loading } from "../../assets/images/loader.svg";

// Utils
import { timeFormatting } from "../utils/toolsTime";
import {
  getDataWeatherValidLocalStorage,
  setDataWeatherLocalStorage,
  getUpdatedAtLocalStorage,
} from "../utils/toolsLocalStorage";

//Api
import { getWeather } from "../../services/api";

const TemperatureCard = ({ city, nameCityShow, setNameCityShow, idx }) => {
  const [updatedTemp, setUpdatedTemp] = useState(true);
  const [tryAgain, setTryAgain] = useState(false);
  const [weather, setWeather] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [updatedAt, setUpdatedAt] = useState("Loading...");

  setInterval(() => {
    if (!isLoading && !tryAgain) {
      const date = new Date();
      let dataWeather = getDataWeatherValidLocalStorage(city, date);
      if (!dataWeather) {
        setUpdatedTemp(true);
      }
    }
  }, 5000);

  const fetchWeather = async () => {
    setIsLoading(true);
    setUpdatedTemp(false);
    try {
      const date = new Date();

      let dataWeather = getDataWeatherValidLocalStorage(city, date);
      if (!!dataWeather) {
        setUpdatedAt(timeFormatting(getUpdatedAtLocalStorage(city)));
      } else {
        dataWeather = await getWeather(city);
        setDataWeatherLocalStorage(city, dataWeather, date);
        setUpdatedAt(timeFormatting(date));
      }

      setWeather(dataWeather);
      setTryAgain(false);
      setIsLoading(false);
    } catch (error) {
      setTryAgain(true);
      setIsLoading(false);
    }
  };

  useEffect(() => updatedTemp && fetchWeather(), [updatedTemp]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleClickCard = (nameCity) => setNameCityShow(nameCity);

  const handleClickTryAgain = () => setUpdatedTemp(true);
  return (
    <li
      className={`temperature-card-container item-${idx}`}
      onClick={() => handleClickCard(city)}
      key={city}
      data-testid={`temperature-card-${city}`}
    >
      <label className="temperature-card-city">{city}</label>
      {isLoading ? (
        <Loading className="temperature-card-loading" />
      ) : tryAgain ? (
        <TryAgain onClick={handleClickTryAgain} />
      ) : (
        <>
          <Details
            temp={Math.trunc(weather?.main?.temp)}
            pressure={weather?.main?.pressure}
            humidity={weather?.main?.humidity}
            showHumidityPressure={nameCityShow === city}
            updatedAt={updatedAt}
          />
          <label className="temperature-card-footer">
            {`Updated at ${updatedAt}`}
          </label>
        </>
      )}
    </li>
  );
};

export default TemperatureCard;
