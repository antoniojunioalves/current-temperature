// External libs
import React from "react";

// CSS
import "./tryAgain.scss";

const TryAgain = ({ onClick }) => {
  return (
    <div data-testid="try-again" className="try-again-container">
      <label>Someting went wrong</label>
      <button data-testid="try-again-button" onClick={() => onClick()}>
        Try Again
      </button>
    </div>
  );
};

export default TryAgain;
