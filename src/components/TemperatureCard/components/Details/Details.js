// External libs
import React from "react";

// CSS
import "./details.scss";

// Utils
import { getStyleTemperature } from "../../../utils/getColorTemperature";

const Details = ({ pressure, humidity, temp, showHumidityPressure }) => {
  const descriptionDetails = (title, value, unitMeasurement) => {
    return (
      <div className="details-description">
        <label className="details-description-title">{title}</label>
        <label>
          {value}
          <label className="details-description-unit">{unitMeasurement}</label>
        </label>
      </div>
    );
  };

  return (
    <>
      <label className={`details-temp ${getStyleTemperature(temp)}`}>
        {temp}
        <label className={`details-temp ${getStyleTemperature(temp)}`}>°</label>
      </label>
      {showHumidityPressure && (
        <div className="details-container">
          {descriptionDetails("HUMIDITY", humidity, "%")}
          {descriptionDetails("PRESSURE", pressure, "hPa")}
        </div>
      )}
    </>
  );
};

export default Details;
