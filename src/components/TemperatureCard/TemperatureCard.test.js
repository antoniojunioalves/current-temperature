// External libs
import { render, waitFor, fireEvent } from "@testing-library/react";

// Component
import TemperatureCard from "./TemperatureCard";

// fetch mock
import fetchMock from "fetch-mock";

const mockApiNairobi =
  "http://api.openweathermap.org/data/2.5/weather?q=Nairobi,%20KE&units=metric&APPID=c8d3490380afd9d61df0e0cdffedbd26";

const responseNairobi = {
  coord: { lon: 36.8167, lat: -1.2833 },
  weather: [
    { id: 803, main: "Clouds", description: "broken clouds", icon: "04d" },
  ],
  base: "stations",
  main: {
    temp: 21.73,
    feels_like: 22.13,
    temp_min: 20,
    temp_max: 24,
    pressure: 1016,
    humidity: 83,
  },
  visibility: 10000,
  wind: { speed: 3.44, deg: 110, gust: 5.37 },
  clouds: { all: 75 },
  dt: 1619966290,
  sys: {
    type: 1,
    id: 2558,
    country: "KE",
    sunrise: 1619926056,
    sunset: 1619969500,
  },
  timezone: 10800,
  id: 184745,
  name: "Nairobi",
  cod: 200,
};

describe("Component <TemperatureCard />", () => {
  afterEach(() => fetchMock.reset());
  let props;

  beforeEach(() => {
    props = {
      city: "Nairobi, KE",
      nameCityShow: "Nairobi, KE",
      setNameCityShow: jest.fn(),
    };
  });

  const renderComponent = () => {
    const component = render(<TemperatureCard {...props} />);
    return component;
  };

  test("should render component", () => {
    const component = renderComponent();
    expect(component).toBeDefined();
  });

  test("Load city Nairobi, KE", async () => {
    fetchMock.mock(mockApiNairobi, responseNairobi);

    const { getByTestId } = renderComponent();

    await waitFor(() => {
      expect(getByTestId("temperature-card-Nairobi, KE")).toBeInTheDocument();
    });
  });

  test("Load city Nairobi, KE with error(Not send city) show Try Again", async () => {
    fetchMock.mock(mockApiNairobi, responseNairobi);

    props = {
      ...props,
      city: undefined,
    };

    const { getByTestId } = renderComponent();

    await waitFor(() => {
      const divTryAgain = getByTestId("try-again");
      expect(divTryAgain).toBeInTheDocument();
    });
  });

  test("Click card", async () => {
    fetchMock.mock(mockApiNairobi, responseNairobi);

    const setNameCityShow = jest.fn();

    props = {
      ...props,
      setNameCityShow,
    };

    const { getByTestId } = renderComponent();

    await waitFor(() => {
      const liCard = getByTestId("temperature-card-Nairobi, KE");
      fireEvent.click(liCard);

      expect(setNameCityShow).toHaveBeenCalledWith("Nairobi, KE");
    });
  });

  test("Click Try Again", async () => {
    fetchMock.mock(mockApiNairobi, responseNairobi);
    props = {
      ...props,
      city: undefined,
    };

    const { getByTestId } = renderComponent();

    await waitFor(() => {
      const buttonTryAgain = getByTestId("try-again-button");
      fireEvent.click(buttonTryAgain);
      // TODO: Não consegui fazer a verificação se a API realmente foi acionada,
      // ou quebrar a chamada de um jeito diferente sem ser não passando a cidade, para o Try Again aparecer eu clicar no botão
      // e então conseguir testar se o componente foi renderizado com os dados do clima.

      // expect(fetchMock).toHaveBeenCalledWith(mockApiNairobi);
    });
  });
});
