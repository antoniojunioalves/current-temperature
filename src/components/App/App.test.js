// External libs
import { render } from "@testing-library/react";

// Component
import App from "./App";

describe("Component <App />", () => {
  test("should render component", () => {
    const component = render(<App />);
    expect(component).toBeDefined();
  });

  test("verify render Header", () => {
    const { getByTestId } = render(<App />);

    const imgLogo = getByTestId("header-logo");

    expect(imgLogo).toBeInTheDocument();
  });

  test("verify render List", () => {
    const { getByTestId } = render(<App />);

    const ulList = getByTestId("temperature-list");

    expect(ulList).toBeInTheDocument();
  });
});
