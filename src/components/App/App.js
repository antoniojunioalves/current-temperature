// External libs
import React from "react";

// CSS
import "./app.scss";

// Components
import TemperatureList from "../TemperatureList";
import Header from "../Header";

const App = () => {
  return (
    <main className="app-container">
      <Header />
      <TemperatureList />
    </main>
  );
};

export default App;
