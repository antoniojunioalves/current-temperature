import { differenceInSeconds } from "date-fns";
import { objIsNull } from "./toolsObject";

const getDataWeatherValidLocalStorage = (city, dateNow) => {
  try {
    const updatedAt = localStorage.getItem(`updatedAt${city}`);
    const dataWeather = localStorage.getItem(`dataWeather${city}`);

    if (!updatedAt || !dataWeather) {
      return;
    }

    const dateLocalStorage = new Date(updatedAt);

    const diffSeconds = differenceInSeconds(dateNow, dateLocalStorage);
    if (diffSeconds <= 600) {
      if (objIsNull(dataWeather)) {
        return;
      }
      return JSON.parse(dataWeather);
    }
  } catch (error) {
    localStorage.setItem(`updatedAt${city}`, "");
    localStorage.setItem(`dataWeather${city}`, "");
  }

  return;
};

const getUpdatedAtLocalStorage = (city) => {
  const dateLocalStorage = new Date(localStorage.getItem(`updatedAt${city}`));

  return new Date(dateLocalStorage);
};

const setDataWeatherLocalStorage = (city, dataWeather, updatedAt) => {
  if (updatedAt) {
    localStorage.setItem(`updatedAt${city}`, updatedAt);
  }
  if (dataWeather) {
    localStorage.setItem(`dataWeather${city}`, JSON.stringify(dataWeather));
  }
};

export {
  getDataWeatherValidLocalStorage,
  setDataWeatherLocalStorage,
  getUpdatedAtLocalStorage,
};
