const objIsNull = (obj) => {
  if (
    obj === "undefined" ||
    obj === undefined ||
    obj === null ||
    obj === "{}" ||
    typeof JSON.parse(obj) !== "object"
  ) {
    return true;
  }

  return false;
};

export { objIsNull };
