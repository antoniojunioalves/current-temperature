import { getStyleTemperature } from "./getColorTemperature";

describe("utils getColorTemperature", () => {
  test("temperature <=5 blue", () => {
    expect(getStyleTemperature(5)).toEqual("details-temp-blue");
  });

  test("temperature >5 and <=25 orange", () => {
    expect(getStyleTemperature(6)).toEqual("details-temp-orange");
    expect(getStyleTemperature(25)).toEqual("details-temp-orange");
  });

  test("temperature >25 red", () => {
    expect(getStyleTemperature(26)).toEqual("details-temp-red");
  });
});
