import { objIsNull } from "./toolsObject";

describe("utils toolsObject", () => {
  test("object with undefined", () => {
    const objTeste = undefined;

    expect(objIsNull(objTeste)).toEqual(true);
  });

  test("object with 'undefined'", () => {
    const objTeste = "undefined";

    expect(objIsNull(objTeste)).toEqual(true);
  });

  test("object with null", () => {
    const objTeste = null;

    expect(objIsNull(objTeste)).toEqual(true);
  });
  test("object with '{}'", () => {
    const objTeste = "{}";

    expect(objIsNull(objTeste)).toEqual(true);
  });

  test("object with type different object", () => {
    const objTeste = "1234";

    expect(objIsNull(objTeste)).toEqual(true);
  });

  test("object valid", () => {
    const objTeste = {
      coord: { lon: -51.7216, lat: 64.1835 },
      weather: [
        { id: 801, main: "Clouds", description: "few clouds", icon: "02d" },
      ],
      base: "stations",
      main: {
        temp: 5.78,
        feels_like: 2.75,
        temp_min: 5.56,
        temp_max: 6,
        pressure: 1015,
        humidity: 52,
      },
      visibility: 10000,
      wind: { speed: 4.12, deg: 30 },
      clouds: { all: 20 },
      dt: 1619982432,
      sys: {
        type: 1,
        id: 86,
        country: "GL",
        sunrise: 1619938455,
        sunset: 1619999597,
      },
      timezone: -7200,
      id: 3421319,
      name: "Nuuk",
      cod: 200,
    };

    expect(objIsNull(JSON.stringify(objTeste))).toEqual(false);
  });
});
