const getStyleTemperature = (temperature) => {
  if (temperature <= 5) {
    return "details-temp-blue";
  } else if (temperature > 5 && temperature <= 25) {
    return "details-temp-orange";
  } else {
    return "details-temp-red";
  }
};

export { getStyleTemperature };
