// External libs
import React, { useState } from "react";

// Components
import TemperatureCard from "../TemperatureCard";

// CSS
import "./temperatureList.scss";

const listWheater = ["Nuuk, GL", "Urubici, BR", "Nairobi, KE"];

const TemperatureList = () => {
  const [nameCityShow, setNameCityShow] = useState("Urubici, BR");

  return (
    <ul data-testid="temperature-list" className="temperature-list">
      {listWheater.map((nameCity, idx) => (
        <TemperatureCard
          idx={idx}
          key={nameCity}
          city={nameCity}
          nameCityShow={nameCityShow}
          setNameCityShow={setNameCityShow}
        />
      ))}
    </ul>
  );
};

export default TemperatureList;
