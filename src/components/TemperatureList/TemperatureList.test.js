// External libs
import { render } from "@testing-library/react";

// Component
import TemperatureList from "./TemperatureList";

describe("Component <TemperatureList />", () => {
  test("should render component", () => {
    const component = render(<TemperatureList />);
    expect(component).toBeDefined();
  });

  test("Exists 3 cities", () => {
    const listWheater = ["Nuuk, GL", "Urubici, BR", "Nairobi, KE"];

    const { getByTestId } = render(<TemperatureList />);

    listWheater.forEach((city) => {
      const liCard = getByTestId(`temperature-card-${city}`);

      expect(liCard).toBeInTheDocument();
    });
  });
});
